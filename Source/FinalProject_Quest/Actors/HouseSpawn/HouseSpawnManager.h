// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HouseSpawnManager.generated.h"

UCLASS()
class FINALPROJECT_QUEST_API AHouseSpawnManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHouseSpawnManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		TSubclassOf<class AStaticMeshActor> HouseClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		TArray<FVector> HouseLocations;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
