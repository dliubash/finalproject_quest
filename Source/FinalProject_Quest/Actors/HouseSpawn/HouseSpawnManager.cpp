// Fill out your copyright notice in the Description page of Project Settings.


#include "HouseSpawnManager.h"
#include <Engine/World.h>
#include <Engine/StaticMeshActor.h>

// Sets default values
AHouseSpawnManager::AHouseSpawnManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AHouseSpawnManager::BeginPlay()
{
	Super::BeginPlay();

	int HouseNum = HouseLocations.Num();

	for (FVector HouseLoc : HouseLocations)
	{
		//FVector Location(0.0f, 0.0f, 0.0f);
		//FRotator Rotation(0.0f, 0.0f, 0.0f);
		FActorSpawnParameters SpawnInfo;
		GetWorld()->SpawnActor(HouseClass, &HouseLoc, &FRotator::ZeroRotator, SpawnInfo);
	}
}

// Called every frame
void AHouseSpawnManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

