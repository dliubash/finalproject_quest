// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemySpawnManager.h"
#include <Engine/World.h>
#include "AI/Enemy/EnemyChar.h"

// Sets default values
AEnemySpawnManager::AEnemySpawnManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AEnemySpawnManager::BeginPlay()
{
	Super::BeginPlay();

	int EnemyNum = EnemyLocations.Num();

	for (FVector EnemyLoc : EnemyLocations)
	{
		//FVector Location(0.0f, 0.0f, 0.0f);
		//FRotator Rotation(0.0f, 0.0f, 0.0f);
		FActorSpawnParameters SpawnInfo;
		GetWorld()->SpawnActor(EnemyClass,&EnemyLoc, &FRotator::ZeroRotator, SpawnInfo);
	}
	
}

// Called every frame
void AEnemySpawnManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

