// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EnemySpawnManager.generated.h"

UCLASS()
class FINALPROJECT_QUEST_API AEnemySpawnManager : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AEnemySpawnManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		TSubclassOf<class AEnemyChar> EnemyClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		TArray<FVector> EnemyLocations;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
