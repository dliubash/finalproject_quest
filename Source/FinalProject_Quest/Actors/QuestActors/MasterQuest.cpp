// Fill out your copyright notice in the Description page of Project Settings.


#include "MasterQuest.h"

// Sets default values
AMasterQuest::AMasterQuest()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMasterQuest::BeginPlay()
{
	Super::BeginPlay();
	
}

void AMasterQuest::UpdateSubGoals()
{
	CurrentGoals.Empty(); // CLEAR

	for (auto CurrentGoalIndex : CurrentGoalIndices)
	{
		CurrentGoals.Add(QuestInfo.Subgoals[CurrentGoalIndex]);
	}
}




void AMasterQuest::SetupStartingGoals()
{
	CurrentGoalIndices.Empty();
	CurrentDescription = QuestInfo.Description;

	for (auto StartSubGoalIndex : StartingSubGoalIndices)
	{
		AddGoalForIndex(StartSubGoalIndex);
	}

	CurrentHuntedAmounts.SetNum(CurrentGoalIndices.Num());
	UpdateSubGoals();

}


bool AMasterQuest::GoToNextSubGoal()
{
	int32 NextInd = FMath::Max<int32>(CurrentGoalIndices) + 1;
	if (NextInd <= QuestInfo.Subgoals.Num() - 1)
	{
		CurrentGoalIndices.Empty();
		CurrentGoalIndices.Add(NextInd);
		UpdateSubGoals();
		return true;
	}	
	return false;
}

FSGoalInfo AMasterQuest::GoalAtIndex(int Index)
{
	return QuestInfo.Subgoals[Index];
}

/*
void AMasterQuest::OnGoalCompleted(int GoalIndex)
{

}
*/
/*
bool AMasterQuest::IsGoalAlreadyFound(int GoalIndex)
{
	if (GoalAtIndex(GoalIndex).Type == EGoalTypes::GT_Find)
	{

	}

	return false;
}
*/
/*
bool AMasterQuest::CompleteSubGoal(int SubGoalIndex)
{
	int LocalGoalIndex = SubGoalIndex;
	if (CurrentGoalIndices.Contains(LocalGoalIndex))
	{
		FSGoalInfo LocalCompletedGoal = QuestInfo.Subgoals[LocalGoalIndex];
		CompletedSubGoals.Add(LocalCompletedGoal);
		CurrentGoals.Remove(LocalCompletedGoal);
		int LocalWidgetIndex = CurrentGoalIndices.Find(LocalGoalIndex);
		CurrentGoalIndices.Remove(LocalGoalIndex);
	}
	return false;
}
*/
// Called every frame
void AMasterQuest::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

