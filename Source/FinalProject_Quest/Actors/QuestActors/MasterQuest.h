// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Enum/MyEnums.h"
#include "MasterQuest.generated.h"

UCLASS()
class FINALPROJECT_QUEST_API AMasterQuest : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AMasterQuest();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
		void UpdateSubGoals();

	//UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	//	void SetupStartingGoals2();

	UFUNCTION(BlueprintCallable)
		void SetupStartingGoals();

	UFUNCTION()
		bool GoToNextSubGoal();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		bool CompleteSubGoal(bool bIsFailed, int SubGoalIndex);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		bool AddGoalForIndex(int Index);

	UFUNCTION(BlueprintCallable, BlueprintPure)
		FSGoalInfo GoalAtIndex(int Index);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "QuestInformation")
		FSQuestInfo QuestInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "QuestInformation")
		TArray<int32> StartingSubGoalIndices = { 0 };

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void OnGoalCompleted(int GoalIndex);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void OnGoalFailed(int GoalIndex);

	//	UFUNCTION(BlueprintCallable, BlueprintPure)
	//		bool IsGoalAlreadyFound(int GoalIndex);
	//private:
		// do not touch in children!
	UPROPERTY(BlueprintReadWrite, meta = (AllowPrivateAccess = true))
		TArray<int32> CurrentGoalIndices;
	UPROPERTY(BlueprintReadWrite, meta = (AllowPrivateAccess = true))
		TArray<FSGoalInfo> CurrentGoals;
	UPROPERTY(BlueprintReadWrite, meta = (AllowPrivateAccess = true))
		TArray<FSCompletedGoal> CompletedSubGoals;
	UPROPERTY(BlueprintReadWrite, meta = (AllowPrivateAccess = true))
		TArray<int> CurrentHuntedAmounts;
	UPROPERTY(BlueprintReadWrite, meta = (AllowPrivateAccess = true))
		int SelectedSubGoalIndex;
	UPROPERTY(BlueprintReadWrite, meta = (AllowPrivateAccess = true))
		EQuestStates CurrentState;
	UPROPERTY(BlueprintReadWrite, meta = (AllowPrivateAccess = true))
		FText CurrentDescription;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
