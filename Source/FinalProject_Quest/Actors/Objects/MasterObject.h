// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MasterObject.generated.h"

UCLASS()
class FINALPROJECT_QUEST_API AMasterObject : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMasterObject();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintImplementableEvent)
		void SetupWidget();

	UPROPERTY(VisibleAnywhere)
		class UStaticMeshComponent* StaticMesh;

	UPROPERTY(BlueprintReadWrite,EditAnywhere)
		FText Name;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
