// Fill out your copyright notice in the Description page of Project Settings.


#include "MasterObject.h"
#include <Components/StaticMeshComponent.h>

// Sets default values
AMasterObject::AMasterObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	RootComponent = StaticMesh;

	//StaticMesh->SetNotifyRigidBodyCollision(true);
	//StaticMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	//StaticMesh->SetGenerateOverlapEvents(false);

}

// Called when the game starts or when spawned
void AMasterObject::BeginPlay()
{
	Super::BeginPlay();

	SetupWidget();
	
}

// Called every frame
void AMasterObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

