// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MyGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class FINALPROJECT_QUEST_API AMyGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
		void SetupGamePaused(bool bBecomePaused);
		void ToggleGamePaused();

		virtual void BeginPlay() override;

protected:
	bool bShouldBecomePaused = false;
};
