// Fill out your copyright notice in the Description page of Project Settings.


#include "MyGameModeBase.h"
#include <Engine/Engine.h>
#include <Engine/World.h>
#include <GameFramework/PlayerController.h>


	//PlayerInputComponent->BindAction("Pause", IE_Pressed, this, &AMyGameModeBase::SetupGamePaused);
void AMyGameModeBase::SetupGamePaused(bool bBecomePaused)
{
	APlayerController* const MyPlayer = Cast<APlayerController>(GEngine->GetFirstLocalPlayerController(GetWorld()));
	if (MyPlayer != NULL)
	{
		MyPlayer->SetPause(bBecomePaused);
	}
}

void AMyGameModeBase::ToggleGamePaused()
{
	bShouldBecomePaused = !bShouldBecomePaused;
	SetupGamePaused(bShouldBecomePaused);
}

void AMyGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	UWorld* World = GetWorld();
	if (World) {
		World->GetFirstPlayerController()->InputComponent->BindAction("Pause", IE_Pressed, this, &AMyGameModeBase::ToggleGamePaused).bExecuteWhenPaused = true;
	}

}
