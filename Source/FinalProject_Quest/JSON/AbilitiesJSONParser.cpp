// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilitiesJSONParser.h"
#include "Enum/MyEnums.h"
#include <FileHelper.h>
#include <Paths.h>
#include <JsonUtilities/Public/JsonObjectConverter.h>

// Sets default values
AAbilitiesJSONParser::AAbilitiesJSONParser()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AAbilitiesJSONParser::BeginPlay()
{
	Super::BeginPlay();

	//ReadAbilitiesJSON(AbilitiesArray);
}

// Called every frame
void AAbilitiesJSONParser::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AAbilitiesJSONParser::ReadAbilitiesJSON(TArray<FSAbilityTest> &AbilArr)
{
	const FString JsonFilePath = FPaths::ConvertRelativePathToFull(FPaths::ProjectContentDir()) + "Data/DataAbilities.json";
	FString JsonArrayString; //Json converted to FString

	FFileHelper::LoadFileToString(JsonArrayString, *JsonFilePath);


	FJsonObjectConverter::JsonArrayStringToUStruct(JsonArrayString, &AbilArr, 0, 0);

	if (AbilArr.Num() > 0)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Var equals %s"), *AbilArr[0].Description));
	}
	else
	{
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Path equals %s"), *JsonFilePath));
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("JsonArrayString equals %s"), *JsonArrayString));

	}
}

