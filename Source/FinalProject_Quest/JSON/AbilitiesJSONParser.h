// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Enum/MyEnums.h"
#include "AbilitiesJSONParser.generated.h"

USTRUCT(BlueprintType)
struct FSAbilityTest
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Level = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EAbilityNames AbilityName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bDescrShouldBeAddedToControls;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString Description;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString LongDescriptionShownOnce;
	// changing vars just does not work
	//UPROPERTY(BlueprintReadWrite)
	//	bool bWasActivated = false;
};

UCLASS()
class FINALPROJECT_QUEST_API AAbilitiesJSONParser : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAbilitiesJSONParser();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	TArray<FSAbilityTest> AbilitiesArray;

	static void ReadAbilitiesJSON(TArray<FSAbilityTest> &AbilArr);
};
