// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FinalProject_QuestGameMode.generated.h"

UCLASS(minimalapi)
class AFinalProject_QuestGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AFinalProject_QuestGameMode();
};



