// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Animation/AnimMontage.h"
#include "Actors/Objects/MasterObject.h"
#include "Yin.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNoParamDelegateChar);

UCLASS()
class FINALPROJECT_QUEST_API AYin : public ACharacter
{
	GENERATED_BODY()

		/** Camera boom positioning the camera behind the character */
		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FollowCamera;

public:
	// Sets default values for this character's properties
	AYin();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;

	// Level and experience info
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UXPComponent* XPComponent;
	// new abilities/rewards on level ups
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UAbilityComponent* AbilityComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UVitalityComp* VitalityComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UBackpackComp* BackpackComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UCapsuleComponent* WeaponCapsuleComp;
	UPROPERTY(EditDefaultsOnly, Category = "MeleeWeapon")
		FName WeaponSocket;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	UFUNCTION()
		void MeleeAttack();
	UFUNCTION()
		void AoeAttack();

	// TESTING PURPOSES!!!!
	UFUNCTION()
		void TestXPPoints();

	// interact with NPC
	UFUNCTION(BlueprintImplementableEvent)
		void InteractWithNPC();

	UFUNCTION()
		void CharacterDied();

	// should be false until getting corresponding knowledge
	UPROPERTY(BlueprintReadWrite, Category = "Attack")
		bool bCanUseMeleeAttack = false;
	UPROPERTY(BlueprintReadWrite, Category = "Attack")
		bool bCanUseAoeAttack = false;
	UPROPERTY(BlueprintReadWrite, Category = "Build")
		bool bCanBuildHouse = false;

	UPROPERTY(BlueprintReadWrite, Category = "Attack")
		bool bIsAttacking = false;
	UPROPERTY(BlueprintReadWrite, Category = "Attack")
		bool bIsMeleeAttacking = false;
	UPROPERTY(BlueprintReadWrite, Category = "Attack")
		bool bIsAoeAttacking = false;

	UPROPERTY(EditDefaultsOnly, Category = "Montages")
		UAnimMontage* MeleeAttackMontage;
	UPROPERTY(EditDefaultsOnly, Category = "Montages")
		UAnimMontage* AoeAttackMontage;

	//UPROPERTY(BlueprintAssignable, Category = "Delegates")
	//	FNoParamDelegateChar OnAttackBegin;

	UPROPERTY(BlueprintAssignable, Category = "Delegates")
		FNoParamDelegateChar OnCharMove;

	UPROPERTY(BlueprintReadWrite)
		TArray<TSubclassOf<AMasterObject>> ObtainedObjects;

	UPROPERTY(EditDefaultsOnly, Category = "Damage")
		float MeleeDamage = 20;
	UPROPERTY(EditDefaultsOnly, Category = "Damage")
		float AoeDamage = 10;
	UPROPERTY(EditDefaultsOnly, Category = "Damage")
		float DamageRadius = 400;

	UFUNCTION()
		void OnOverlapApplyMeleeDamage(class UPrimitiveComponent* HitComp, AActor * Other, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex,
			bool bFromSweep, const FHitResult & Hit);
	UFUNCTION()
		void OnOverlapMeleeEnd(class UPrimitiveComponent* HitComp, AActor * Other, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex);
	//	void OnOverlapEnd(AActor * Other, AActor * Other2);
	bool bIsWeaponOverlappingEnemy = false;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void ResetAttack();

	UFUNCTION(BlueprintCallable)
		void SetCanUseMeleeAttack(bool bCanUseMelee);
	UFUNCTION(BlueprintCallable)
		void SetCanUseAoeAttack(bool bCanUseAoe);
	UFUNCTION(BlueprintCallable)
		void CharacterRespawned();
	UFUNCTION(BlueprintCallable)
		void SetCanBuildHouse(bool bCanBuild);

	void ApplyRadDamage();

	float GetMeleeDamage();
	void SetMeleeDamage(float Damage);

	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

};
