// Fill out your copyright notice in the Description page of Project Settings.


#include "AbilityComponent.h"
#include "Enum/MyEnums.h"
#include <Array.h>
#include "../Yin.h"
#include "Vitality/VitalityComp.h"
#include "JSON/AbilitiesJSONParser.h"
#include <Engine/World.h>


// Sets default values for this component's properties
UAbilityComponent::UAbilityComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UAbilityComponent::BeginPlay()
{
	Super::BeginPlay();

	MyChar = Cast<AYin>(GetOwner());
	if (MyChar)
	{
		XPComp = MyChar->XPComponent;
	}
	
	if (XPComp)
	{
		XPComp->OnLevelUp.AddDynamic(this, &UAbilityComponent::AddAbilities);
	}
	//AddedAbilities.Empty();
	TArray<FSAbilityTest> AbilArrFromJSON;
	AAbilitiesJSONParser::ReadAbilitiesJSON(AbilArrFromJSON);

	AbilitiesInfo.Abilities.Empty();

	for (FSAbilityTest OneAbilityTest : AbilArrFromJSON)
	{
		FSAbility OneAbility (OneAbilityTest.Level, OneAbilityTest.AbilityName, 
			OneAbilityTest.bDescrShouldBeAddedToControls, OneAbilityTest.Description, OneAbilityTest.LongDescriptionShownOnce);
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Var equals %d"), OneAbility.Level));

		AbilitiesInfo.Abilities.Add(OneAbility);
	}
	/*
	for (auto ab : AbilitiesInfo.Abilities)
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Var equals %d"), ab.Level));
	}
	*/
}


void UAbilityComponent::AddAbilities()
{
	int CurrentLevel = XPComp->GetLevel();
	for (FSAbility Ability : AbilitiesInfo.Abilities)
	{
		if (Ability.Level <= CurrentLevel /* && !Ability.bWasActivated*/)
		{
			bool bAbilityFound = false;
			for (auto AddAbility : AddedAbilities)
			{
				if (AddAbility == Ability.AbilityName)
				{
					bAbilityFound = true;
					break;
				}
			}
			
			if (!bAbilityFound)
			{
				AddedAbilities.Add(Ability.AbilityName);
				//GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, FString::Printf(TEXT("New level equals %d"), CurrentLevel));

				OnNewAbility.Broadcast(Ability.Description, Ability.bDescrShouldBeAddedToControls,Ability.LongDescriptionShownOnce);

				switch (Ability.AbilityName)
				{
				case EAbilityNames::AN_AnimalFriend:
					AddAbilityAnimalFriend();
					break;
				case EAbilityNames::AN_Autoregeneration :
					AddAbilityAutoregeneration();
					break;
				case EAbilityNames::AN_WeaponUsage1:
					AddAbilityWeaponUsage1();
					break;
				case EAbilityNames::AN_WeaponUsage2:
					AddAbilityWeaponUsage2();
					break;
				case EAbilityNames::AN_HouseBack:
					AddAbilityHouseBack();
					break;
				}

			}

		}
	}
	
}

void UAbilityComponent::AddAbilityAnimalFriend()
{
	//GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, FString::Printf(TEXT("AnimalFriend")));
	FVector Direction = MyChar->GetActorForwardVector();
	FVector Location = MyChar->GetActorLocation() + Direction * 300;
	FRotator Rotation = MyChar->GetActorRotation();
	FTransform TmpTransform = FTransform(Rotation, Location);
	if (AnimalFriendClass)
	{
		GetWorld()->SpawnActor(AnimalFriendClass, &TmpTransform);
	}
}

void UAbilityComponent::AddAbilityAutoregeneration()
{
	//GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, FString::Printf(TEXT("Auto regeneration")));
	if (MyChar->VitalityComp)
	{
		MyChar->VitalityComp->SetAutoRegeneration(true);
		MyChar->VitalityComp->AddHealth(MyChar->VitalityComp->GetMaxHealth());
	}
}

void UAbilityComponent::AddAbilityWeaponUsage1()
{
	MyChar->SetCanUseMeleeAttack(true);
	//GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, FString::Printf(TEXT("AddAbilityWeaponUsage1")));
}

void UAbilityComponent::AddAbilityWeaponUsage2()
{
	MyChar->SetCanUseAoeAttack(true);
	//GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, FString::Printf(TEXT("AddAbilityWeaponUsage2")));
}

void UAbilityComponent::AddAbilityHouseBack()
{
	//GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, FString::Printf(TEXT("AddAbilityYinsTogether")));
	MyChar->SetCanBuildHouse(true);
}

// Called every frame
void UAbilityComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

