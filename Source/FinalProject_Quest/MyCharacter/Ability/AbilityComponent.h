// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "../Yin.h"
#include "../XP/XPComponent.h"
#include "Enum/MyEnums.h"
#include "AI/AnimalFriend/AnimalFriend.h"
#include "AbilityComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FDescrThreeParamDel, FText, Description, bool, bDescrShouldBeAddedToControls, FText, LongDescriptionShownOnce);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FINALPROJECT_QUEST_API UAbilityComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UAbilityComponent();

	UPROPERTY(BlueprintAssignable, Category = "Delegates")
		FDescrThreeParamDel OnNewAbility;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION()
		void AddAbilities();

	void AddAbilityAnimalFriend();
	void AddAbilityAutoregeneration();
	void AddAbilityWeaponUsage1();
	void AddAbilityWeaponUsage2();
	void AddAbilityHouseBack();

	AYin* MyChar;
	UXPComponent* XPComp;

	UPROPERTY()
		FSAbilitiesInfo AbilitiesInfo;

	TArray<EAbilityNames> AddedAbilities;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<class AAnimalFriend> AnimalFriendClass;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
