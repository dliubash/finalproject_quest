// Fill out your copyright notice in the Description page of Project Settings.


#include "Yin.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"
#include "XP/XPComponent.h"
#include "Vitality/VitalityComp.h"
#include "Ability/AbilityComponent.h"
#include "AI/Enemy/EnemyChar.h"
#include "Backpack/BackpackComp.h"
#include "Core/MyGameModeBase.h"

// Sets default values
AYin::AYin()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	GetMesh()->AddLocalTransform(FTransform(FRotator(0, -90, 0).Quaternion(), FVector(0, 0, -90)));

	XPComponent = CreateDefaultSubobject<UXPComponent>(TEXT("XPComponent"));
	AbilityComp = CreateDefaultSubobject<UAbilityComponent>(TEXT("AbilityComponent"));

	VitalityComp = CreateDefaultSubobject<UVitalityComp>(TEXT("VitalityComponent"));
	VitalityComp->OnOwnerDied.AddDynamic(this, &AYin::CharacterDied);

	WeaponCapsuleComp = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CapsuleWeaponCollision"));

	BackpackComp = CreateDefaultSubobject<UBackpackComp>(TEXT("BackpackComponent"));

}

// Called when the game starts or when spawned
void AYin::BeginPlay()
{
	Super::BeginPlay();

	UGameplayStatics::GetPlayerController(GetWorld(), 0)->SetInputMode(FInputModeGameAndUI());
	//UGameplayStatics::GetPlayerController(GetWorld(), 0)->bShowMouseCursor = true;
	
	if (!WeaponSocket.IsNone()) {
		WeaponCapsuleComp->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform /*FAttachmentTransformRules::SnapToTargetIncludingScale*/, WeaponSocket);
	}

	WeaponCapsuleComp->OnComponentBeginOverlap.AddDynamic(this, &AYin::OnOverlapApplyMeleeDamage);
	WeaponCapsuleComp->OnComponentEndOverlap.AddDynamic(this, &AYin::OnOverlapMeleeEnd);
	//OnActorEndOverlap.AddDynamic(this, &AYin::OnOverlapEnd);
}

// Called every frame
void AYin::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AYin::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	//Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AYin::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AYin::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AYin::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AYin::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &AYin::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AYin::TouchStopped);

	// VR headset functionality
	//PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AYin::OnResetVR);

	PlayerInputComponent->BindAction("MeleeAttack", IE_Pressed, this, &AYin::MeleeAttack);
	PlayerInputComponent->BindAction("AoeAttack", IE_Pressed, this, &AYin::AoeAttack);

	PlayerInputComponent->BindAction("InteractWith", IE_Pressed, this, &AYin::InteractWithNPC);

	// TESTING PURPOSES!!!!
	// PlayerInputComponent->BindAction("TestXPPoints", IE_Pressed, this, &AYin::TestXPPoints);

}
	
void AYin::ResetAttack()
{
	bIsAttacking = false;
	bIsMeleeAttacking = false; 
	bIsAoeAttacking = false;

}

void AYin::SetCanUseMeleeAttack(bool bCanUseMelee)
{
	bCanUseMeleeAttack = bCanUseMelee;
}

void AYin::SetCanUseAoeAttack(bool bCanUseAoe)
{
	bCanUseAoeAttack = bCanUseAoe;
}

void AYin::ApplyRadDamage()
{
	UGameplayStatics::ApplyRadialDamage(GetWorld(), AoeDamage,GetActorLocation(),DamageRadius,NULL,{this},this, NULL,true);
	
}

float AYin::GetMeleeDamage()
{
	return MeleeDamage;
}

void AYin::SetMeleeDamage(float Damage)
{
	MeleeDamage = Damage;
}

void AYin::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	Jump();
}

void AYin::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	StopJumping();
}

void AYin::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AYin::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AYin::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);

		OnCharMove.Broadcast();
	}
}

void AYin::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);

		OnCharMove.Broadcast();
	}
}

void  AYin::MeleeAttack()
{
	if (bCanUseMeleeAttack)
	{
		if (!bIsAttacking)
		{
			if (MeleeAttackMontage)
			{
				// used in ABP_Yin as of 04/08/19
				//OnAttackBegin.Broadcast();
				PlayAnimMontage(MeleeAttackMontage);
				bIsAttacking = true;
				bIsMeleeAttacking = true;
				// false for all attacks will be in ANimNotify ResetAttack
			}
			
		}
	}

}

void AYin::AoeAttack()
{
	if (bCanUseAoeAttack)
	{
		if (!bIsAttacking)
		{
			if (AoeAttackMontage)
			{
				// used in ABP_Yin as of 04/08/19
				//OnAttackBegin.Broadcast();
				PlayAnimMontage(AoeAttackMontage);
				bIsAttacking = true;
				bIsAoeAttacking = true;
				// false for all attacks will be in ANimNotify ResetAttack
			}

		}
	}
}

// TESTING PURPOSES!!!!
void AYin::TestXPPoints()
{
	XPComponent->AddXPPoints(100);
}

void AYin::CharacterDied()
{
	DisableInput(GetWorld()->GetFirstPlayerController());
	//GetCharacterMovement()->DisableMovement();
	bUseControllerRotationYaw = false;
	SetCanUseMeleeAttack(false);
	SetCanUseMeleeAttack(false);
}


void AYin::CharacterRespawned()
{
	//GetCharacterMovement()->Movement;
	EnableInput(GetWorld()->GetFirstPlayerController());
	bUseControllerRotationYaw = true;
	SetCanUseMeleeAttack(true);
	SetCanUseMeleeAttack(true);
}

void AYin::SetCanBuildHouse(bool bCanBuild)
{
	bCanBuildHouse = bCanBuild;
}

void AYin::OnOverlapApplyMeleeDamage(class UPrimitiveComponent* HitComp, AActor * Other, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & Hit)
{
	if (Cast<AEnemyChar>(Other))
	{
		if ((!bIsWeaponOverlappingEnemy) && bIsMeleeAttacking == true && VitalityComp->IsAlive())
		{
			bIsWeaponOverlappingEnemy = true;
			UGameplayStatics::ApplyDamage(Other, MeleeDamage, nullptr, this, nullptr);
		}

	}
	//UE_LOG(LogTemp, Warning, TEXT("Troll Overlap"));
	//GEngine->AddOnScreenDebugMessage(-1, 0.5, FColor::Red, TEXT("Overlap Begin"));
}


//void AYin::OnOverlapEnd(AActor * Self, AActor * Other)
void AYin::OnOverlapMeleeEnd(class UPrimitiveComponent* HitComp, AActor * Other, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{
	if (Cast<AEnemyChar>(Other))
	{
		if (bIsWeaponOverlappingEnemy)
		{
			bIsWeaponOverlappingEnemy = false;
		}
	}
	//GEngine->AddOnScreenDebugMessage(-1, 0.5, FColor::Red, TEXT("Overlap End"));
}
