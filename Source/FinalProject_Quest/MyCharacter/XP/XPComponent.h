// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "XPComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNoParamDelegateXP);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class FINALPROJECT_QUEST_API UXPComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UXPComponent();

	UFUNCTION(BlueprintCallable)
		void AddXPPoints(int Amount);

	UPROPERTY(BlueprintAssignable, Category = "Delegates")
		FNoParamDelegateXP OnLevelUp;

	int GetLevel();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = "XP System")
		int CurrentLevel = 1;
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = "XP System")
		int CurrentXP = 0;
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = "XP System")
		int XPForNextLevel = 150;
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = "XP System")
		float NextXPMultiplier = 1.5;

	UPROPERTY(BlueprintAssignable, Category = "Delegates")
		FNoParamDelegateXP OnXPChanged;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
		class USoundBase* SoundLevelUp;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
		class UParticleSystem* ParticleLevelUp;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


};
