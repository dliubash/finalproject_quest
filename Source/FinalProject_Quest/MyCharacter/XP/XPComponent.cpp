// Fill out your copyright notice in the Description page of Project Settings.


#include "XPComponent.h"

#include <Kismet/GameplayStatics.h>
#include "../Yin.h"

// Sets default values for this component's properties
UXPComponent::UXPComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


void UXPComponent::AddXPPoints(int Amount)
{
	CurrentXP += Amount;

	if (CurrentXP >= XPForNextLevel)
	{
		while (CurrentXP >= XPForNextLevel)
		{
			CurrentXP -= XPForNextLevel;
			XPForNextLevel = (int) (XPForNextLevel*NextXPMultiplier);
			CurrentLevel++;
		}
		OnXPChanged.Broadcast();
		OnLevelUp.Broadcast();
		if (SoundLevelUp)
		{
			UGameplayStatics::PlaySoundAtLocation(this, SoundLevelUp, GetOwner()->GetActorLocation());
		}
		
		if (ParticleLevelUp && Cast<AYin>(GetOwner()))
		{
			// FIX!!! (attach to actor)
			//UGameplayStatics::SpawnEmitterAtLocation(this, ParticleLevelUp, GetOwner()->GetActorLocation(), FRotator::ZeroRotator);
			AYin* MyChar = Cast<AYin>(GetOwner());
			UGameplayStatics::SpawnEmitterAttached(ParticleLevelUp, Cast<USceneComponent>(MyChar->GetMesh()), FName("head"), FVector(0, 0, 1), FRotator(0, 0, 0),
				FVector(1.0, 1.0, 1.0),	EAttachLocation::KeepRelativeOffset, true);
			//UGameplayStatics::SpawnEmitterAttached(ParticleLevelUp,  Cast<USceneComponent>(MyChar->GetMesh()));
		}
		
	}
	else
	{
		OnXPChanged.Broadcast();
	}
}

int UXPComponent::GetLevel()
{
	return CurrentLevel;
}

// Called when the game starts
void UXPComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UXPComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

