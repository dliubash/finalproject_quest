// Fill out your copyright notice in the Description page of Project Settings.


#include "BackpackComp.h"
#include "../Yin.h"
#include <GameFramework/Character.h>
#include "../../Vitality/VitalityComp.h"
#include "GameFramework/CharacterMovementComponent.h"

// Sets default values for this component's properties
UBackpackComp::UBackpackComp()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


void UBackpackComp::AddNewBackpackAbility(EBackpackAbilities BackpackAbilityName)
{
	SlotsNum++;

	for (FSBackpackAbility BackpackAbil : BackpackAbilitiesInfo.BackpackAbilities)
	{
		if (BackpackAbilityName == BackpackAbil.BackpackAbilityName)
		{
			OnNewBackpackAbility.Broadcast(BackpackAbil);
			break;
		}
	}

}

// Called when the game starts
void UBackpackComp::BeginPlay()
{
	Super::BeginPlay();

	MyChar = Cast<AYin>(GetOwner());

	SaveInitParams();

	// ...

}


void UBackpackComp::OnBackpackAbilityClicked(FSBackpackAbility BackpackAbility)
{
	if (!(CurrentBackpackAbilityName == BackpackAbility.BackpackAbilityName) || !bIsFirstClickDone)
	{
		if (!bIsFirstClickDone)
		{
			bIsFirstClickDone = true;
		}
		CurrentBackpackAbilityName = BackpackAbility.BackpackAbilityName;
		//GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, FString::Printf(TEXT("Backpack Ability %f"), BackpackAbility.Strength));

		ClearAbilities();
		ApplyAbility(BackpackAbility);
	}
}

void UBackpackComp::SaveInitParams()
{
	// get from Yin
	if (MyChar)
	{
		InitMaxSpeed = MyChar->GetCharacterMovement()->MaxWalkSpeed;
		InitMaxHealth = MyChar->VitalityComp->GetMaxHealth();
		InitMeleeDamage = MyChar->GetMeleeDamage();
	}

}

void UBackpackComp::ClearAbilities()
{
	// setup Yin's params to Init ones
	MyChar->GetCharacterMovement()->MaxWalkSpeed = InitMaxSpeed;
	MyChar->VitalityComp->SetMaxHealth(InitMaxHealth);
	MyChar->SetMeleeDamage(InitMeleeDamage);
}

void UBackpackComp::ApplyAbility(FSBackpackAbility BackpackAbility)
{
	switch (BackpackAbility.BackpackAbilityName)
	{
	case EBackpackAbilities::BA_AddSpeed:
		AddBackpackAbilitySpeed(BackpackAbility.Strength);
		break;
	case EBackpackAbilities::BA_AddMaxHealth:
		AddBackpackAbilityMaxHealth(BackpackAbility.Strength);
		break;
	case EBackpackAbilities::BA_IncreaseMeleeDamage:
		AddBackpackAbilityMeleeDamage(BackpackAbility.Strength);
		break;
	}
}

void UBackpackComp::AddBackpackAbilitySpeed(float Speed)
{
	MyChar->GetCharacterMovement()->MaxWalkSpeed += Speed;
}

void UBackpackComp::AddBackpackAbilityMaxHealth(float Health)
{
	MyChar->VitalityComp->SetMaxHealth(InitMaxHealth + Health);
}

void UBackpackComp::AddBackpackAbilityMeleeDamage(float Damage)
{
	MyChar->SetMeleeDamage(InitMeleeDamage + Damage);
}

// Called every frame
void UBackpackComp::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

