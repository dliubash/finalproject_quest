// Fill out your copyright notice in the Description page of Project Settings.


#include "BasePickup.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "../../Yin.h"

// Sets default values
ABasePickup::ABasePickup()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	RootComponent = StaticMesh;

	StaticMesh->SetNotifyRigidBodyCollision(true);
	StaticMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	StaticMesh->SetGenerateOverlapEvents(true);
}

// Called when the game starts or when spawned
void ABasePickup::BeginPlay()
{
	Super::BeginPlay();

	StaticMesh->OnComponentBeginOverlap.AddDynamic(this, &ABasePickup::OnOverlap);
}

// Called every frame
void ABasePickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABasePickup::OnOverlap(UPrimitiveComponent * HitComp, AActor * Other, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & Hit)
{
	//GEngine->AddOnScreenDebugMessage(-1, 4.0f, FColor::Green,	FString::Printf(TEXT("Hi!")));
	if (Cast<AYin>(Other))
	{
		SpawnEffects(Hit);
		ApplyOverlapLogic(HitComp, Other, OtherComp, OtherBodyIndex, bFromSweep, Hit);
	}

}

void ABasePickup::SpawnEffects(FHitResult EffectHit)
{

	if (EffectSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, EffectSound, EffectHit.ImpactPoint);
	}

	if (EffectParticle)
	{
		UGameplayStatics::SpawnEmitterAtLocation(this, EffectParticle, EffectHit.ImpactPoint, FRotator::ZeroRotator);
	}

}

void ABasePickup::ApplyOverlapLogic(UPrimitiveComponent * HitComp, AActor * Other, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & Hit)
{
}