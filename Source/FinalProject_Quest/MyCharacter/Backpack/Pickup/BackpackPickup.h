// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyCharacter/Backpack/Pickup/BasePickup.h"
#include "Enum/MyEnums.h"
#include "BackpackPickup.generated.h"

/**
 * 
 */
UCLASS()
class FINALPROJECT_QUEST_API ABackpackPickup : public ABasePickup
{
	GENERATED_BODY()
	
protected:
	virtual void ApplyOverlapLogic(UPrimitiveComponent * HitComp, AActor * Other, UPrimitiveComponent * OtherComp,
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult & Hit) override;

	void AddBackpackItem(AActor * Other);

	UPROPERTY(EditDefaultsOnly)
		EBackpackAbilities BackpackAbility;


};
