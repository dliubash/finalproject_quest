// Fill out your copyright notice in the Description page of Project Settings.


#include "BackpackPickup.h"
#include "../../Yin.h"
#include "../BackpackComp.h"

void ABackpackPickup::ApplyOverlapLogic(UPrimitiveComponent * HitComp, AActor * Other, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & Hit)
{
	AddBackpackItem(Other);
}

void ABackpackPickup::AddBackpackItem(AActor * Other)
{
	if (AYin* MyChar = Cast<AYin>(Other))
	{
		if (MyChar->BackpackComp)
		{
			MyChar->BackpackComp->AddNewBackpackAbility(BackpackAbility);
			//GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, FString::Printf(TEXT("Backpack Ability %d"), BackpackAbility));
			Destroy();
		}
	}

	/*
	UE_LOG(LogTemp, Warning, TEXT("%s "), *Other->GetName());

	if (Other->GetComponentByClass(UVitalityComponent::StaticClass()))
	{
		UVitalityComponent* VitalityComp = Cast< UVitalityComponent>(Other->GetComponentByClass(UVitalityComponent::StaticClass()));

		VitalityComp->AddHealth(HealthRecovering);

		Destroy();
	}
	*/
}
