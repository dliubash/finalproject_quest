// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BasePickup.generated.h"

UCLASS(abstract)
class FINALPROJECT_QUEST_API ABasePickup : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABasePickup();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
		void OnOverlap(class UPrimitiveComponent* HitComp, AActor * Other, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex,
			bool bFromSweep, const FHitResult & Hit);

	virtual void SpawnEffects(FHitResult Hit);
	virtual void ApplyOverlapLogic(class UPrimitiveComponent* HitComp, AActor * Other, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex,
		bool bFromSweep, const FHitResult & Hit);

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
		class USoundBase* EffectSound;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
		class UParticleSystem* EffectParticle;

	UPROPERTY(VisibleAnywhere)
		class UStaticMeshComponent* StaticMesh;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
