// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Enum/MyEnums.h"
#include "BackpackComp.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FBackpackOneParamDel, FSBackpackAbility, BackpackAbil);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class FINALPROJECT_QUEST_API UBackpackComp : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UBackpackComp();

	void AddNewBackpackAbility(EBackpackAbilities BackpackAbility);

	UPROPERTY(BlueprintAssignable, Category = "Delegates")
		FBackpackOneParamDel OnNewBackpackAbility;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
		void OnBackpackAbilityClicked(FSBackpackAbility BackpackAbility);

	UPROPERTY(EditDefaultsOnly)
		FSBackpackAbilitiesInfo BackpackAbilitiesInfo;

	// save initial values of Yin's parameters
	// if they changed during game.. hmmm.. then system won't work properly
	void SaveInitParams();
	// remove all abilities of Yin
	void ClearAbilities();
	// add chosen ability
	void ApplyAbility(FSBackpackAbility BackpackAbility);

	void AddBackpackAbilitySpeed(float Speed);
	void AddBackpackAbilityMaxHealth(float Health);
	void AddBackpackAbilityMeleeDamage(float Damage);

	// CurrentBackpackAbilityName is 0 by default
	EBackpackAbilities CurrentBackpackAbilityName;
	bool bIsFirstClickDone = false;
	UPROPERTY(BlueprintReadOnly)
		int SlotsNum = 0;

	float InitMaxSpeed;
	float InitMaxHealth;
	float InitMeleeDamage;

	class AYin* MyChar;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


};
