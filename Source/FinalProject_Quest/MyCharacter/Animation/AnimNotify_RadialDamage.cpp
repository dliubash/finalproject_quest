// Fill out your copyright notice in the Description page of Project Settings.


#include "AnimNotify_RadialDamage.h"
#include "../Yin.h"

void UAnimNotify_RadialDamage::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* AnimMontage)
{
	
	if (AYin* YinChar = Cast<AYin>(MeshComp->GetOwner()))
	{
		YinChar->ApplyRadDamage();
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT("Anim Notify!")));
	}
}
