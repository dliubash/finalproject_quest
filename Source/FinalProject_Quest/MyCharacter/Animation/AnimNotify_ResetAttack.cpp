// Fill out your copyright notice in the Description page of Project Settings.


#include "AnimNotify_ResetAttack.h"
#include "../Yin.h"
#include "Engine.h"

void UAnimNotify_ResetAttack::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* AnimMontage)
{
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT("Anim Notify!")));
	if (AYin* YinChar = Cast<AYin>(MeshComp->GetOwner()))
	{
		YinChar->ResetAttack();

	}
}


