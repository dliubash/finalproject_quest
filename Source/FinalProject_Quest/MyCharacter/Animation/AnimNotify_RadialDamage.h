// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "AnimNotify_RadialDamage.generated.h"

/**
 *
 */
UCLASS()
class FINALPROJECT_QUEST_API UAnimNotify_RadialDamage : public UAnimNotify
{
	GENERATED_BODY()
public:

	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* AnimMontage) override;
};
