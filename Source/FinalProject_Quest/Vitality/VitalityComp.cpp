// Fill out your copyright notice in the Description page of Project Settings.


#include "VitalityComp.h"

// Sets default values for this component's properties
UVitalityComp::UVitalityComp()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


float UVitalityComp::GetMaxHealth()
{
	return MaxHealth;
}

void UVitalityComp::SetMaxHealth(float Health)
{
	MaxHealth = Health;
	if (CurrentHealth > Health)
	{
		CurrentHealth = MaxHealth;
	}
	OnHealthChanged.Broadcast();

	if (bAutoRegeneration)
	{
		GetWorld()->GetTimerManager().ClearTimer(AutoRegenaration_TH);
		//1 for 1 second
		// we should wait RegenerationDelay time
		GetWorld()->GetTimerManager().SetTimer(AutoRegenaration_TH, this, &UVitalityComp::Regenerate, 1, true, RegenerationDelay);
	}
}

float UVitalityComp::GetCurrentHealth()
{
	return CurrentHealth;
}

void UVitalityComp::SetAutoRegeneration(bool bAutoReg)
{
	bAutoRegeneration = bAutoReg;
}

void UVitalityComp::SetCurrentHealth(float Health)
{
	CurrentHealth = Health;
}

void UVitalityComp::AddHealth(float Health)
{
	float NewHealth = FMath::Min(GetCurrentHealth() + Health, GetMaxHealth());
	SetCurrentHealth(NewHealth);
	OnHealthChanged.Broadcast();
	UE_LOG(LogTemp, Warning, TEXT("Actor was recovered to %s"), *FString::SanitizeFloat(NewHealth));
}

// Called when the game starts
void UVitalityComp::BeginPlay()
{
	CurrentHealth = MaxHealth;

	Super::BeginPlay();

	if (GetOwner())
	{
		GetOwner()->OnTakeAnyDamage.AddDynamic(this, &UVitalityComp::DamageHandle);
	}

}

void UVitalityComp::DamageHandle(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	UE_LOG(LogTemp, Warning, TEXT("%s was damaged for %s"), *DamagedActor->GetName(), *FString::SanitizeFloat(Damage));

	if (!IsAlive())
	{
		UE_LOG(LogTemp, Warning, TEXT("Owner Already Dead!"));
		return;
	}

	CurrentHealth -= Damage;
	if (CurrentHealth < 0) 
	{
		CurrentHealth = 0;
	}

	OnHealthChanged.Broadcast();

	if (!IsAlive())
	{
		//Death
		OnOwnerDied.Broadcast();

		if (OnOwnerDiedCauser.IsBound())
		{
			OnOwnerDiedCauser.Broadcast(DamageCauser);
		}

		//if (GetOwner())
		//{
		//	FVector Loc = GetOwner()->GetActorLocation();
		//	OnOwnerDiedLoc.Broadcast(Loc);
		//}

		UE_LOG(LogTemp, Warning, TEXT("OwnerDied!"));
	}
	else if (bAutoRegeneration)
	{
		GetWorld()->GetTimerManager().ClearTimer(AutoRegenaration_TH);
		//1 for 1 second
		// we should wait RegenerationDelay time
		GetWorld()->GetTimerManager().SetTimer(AutoRegenaration_TH, this, &UVitalityComp::Regenerate, 1, true, RegenerationDelay);
	}

}

void UVitalityComp::Regenerate()
{
	if (CurrentHealth > 0)
	{

		if (CurrentHealth < MaxHealth)
		{
			CurrentHealth += FMath::Min(RegenarationRate, MaxHealth - CurrentHealth);
			UE_LOG(LogTemp, Warning, TEXT("Actor has regenerated to %s"), *FString::SanitizeFloat(CurrentHealth));

			OnHealthChanged.Broadcast();
		}
		else
		{
			GetWorld()->GetTimerManager().ClearTimer(AutoRegenaration_TH);
			UE_LOG(LogTemp, Warning, TEXT("Timer of regeneration cleared"));
		}

	}
}

// Called every frame
void UVitalityComp::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool UVitalityComp::IsAlive()
{
	return CurrentHealth > 0;
}
