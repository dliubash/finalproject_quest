// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "VitalityComp.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNoParamDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOneParDelDeathCauser, AActor*, DamageCauser);
//DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOneParamDelegate, FVector, OwnerLocation);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FINALPROJECT_QUEST_API UVitalityComp : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UVitalityComp();


	float GetMaxHealth();
	void SetMaxHealth(float Health);
	float GetCurrentHealth();
	void SetAutoRegeneration(bool bAutoReg);

	UFUNCTION(BlueprintCallable)
		void SetCurrentHealth(float Health);
	UFUNCTION(BlueprintCallable)
		void AddHealth(float Health);

	UPROPERTY(BlueprintAssignable, Category = "Delegates")
		FNoParamDelegate OnOwnerDied;
	UPROPERTY(BlueprintAssignable,Category = "Delegates")
		FOneParDelDeathCauser OnOwnerDiedCauser;

//	UPROPERTY(BlueprintAssignable, Category = "Delegates")
//		FOneParamDelegate OnOwnerDiedLoc;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
		void DamageHandle(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Health")
		float MaxHealth = 100;
	UPROPERTY(BlueprintReadOnly, Category = "Health")
		float CurrentHealth = MaxHealth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		bool bAutoRegeneration = false;
	// regenerated per second
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health", meta = (EditCondition = "bAutoRegeneration"))
		float RegenarationRate = 2;
	// in seconds
	UPROPERTY(BlueprintReadOnly, Category = "Health", meta = (EditCondition = "bAutoRegeneration"))
		float RegenerationDelay = 5;

	FTimerHandle AutoRegenaration_TH;

	UPROPERTY(BlueprintAssignable, Category = "Delegates")
		FNoParamDelegate OnHealthChanged;
	//UPROPERTY(BlueprintAssignable, Category = "Delegates")
	//	FNoParamDelegate OnMaxHealthChanged;


	UFUNCTION()
		void Regenerate();

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool IsAlive();

};
