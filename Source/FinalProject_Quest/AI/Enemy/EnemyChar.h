// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AI/AIChar.h"
#include "Vitality/VitalityComp.h"
#include "EnemyChar.generated.h"

/**
 * 
 */
UCLASS()
class FINALPROJECT_QUEST_API AEnemyChar : public AAIChar
{
	GENERATED_BODY()

public:
	AEnemyChar();

protected:

	UFUNCTION()
		void CharacterDied(AActor* DamageCauser);

	UFUNCTION(BlueprintImplementableEvent)
		void OnCharacterDiedQuestManager(class AYin* YinChar);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UVitalityComp* VitalityComp;

	UPROPERTY(EditDefaultsOnly)
		int XPForKill;
};
