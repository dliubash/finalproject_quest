// Fill out your copyright notice in the Description page of Project Settings.


#include "AnimNotify_MeleeDmg.h"
#include "BearEnemy.h"
#include <Kismet/GameplayStatics.h>

void UAnimNotify_MeleeDmg::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* AnimMontage)
{
	if (ABearEnemy* Bear = Cast<ABearEnemy>(MeshComp->GetOwner()))
	{
		ACharacter* MyChar = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
		if (MyChar)
		{
			float DistanceToPlayer = FVector::Dist(Bear->GetActorLocation(), MyChar->GetActorLocation());
			//GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Yellow, FString::Printf(TEXT("Overlap Begin OK %f"), DistanceToPlayer));
			if (DistanceToPlayer < Bear->DistanceToPlayerForDamage)
			{
				UGameplayStatics::ApplyDamage(MyChar, Bear->GetMeleeDamage(), nullptr, nullptr, nullptr);
			}
		}
	}
}
