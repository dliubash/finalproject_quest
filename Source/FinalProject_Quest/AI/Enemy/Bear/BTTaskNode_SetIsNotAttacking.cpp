// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTaskNode_SetIsNotAttacking.h"
#include <AIController.h>
#include "BearEnemy.h"
#include <Kismet/GameplayStatics.h>

EBTNodeResult::Type UBTTaskNode_SetIsNotAttacking::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	AAIController* AIOwner = OwnerComp.GetAIOwner(); // to get the controller
	APawn* Pawn = AIOwner->GetPawn(); // to get the actor

	if (ABearEnemy* Bear = Cast<ABearEnemy>(Pawn))
	{
		Bear->SetIsMeleeAttacking(false);
		ACharacter* MyChar = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
		if (MyChar)
		{
			float DistanceToPlayer = FVector::Dist(Bear->GetActorLocation(), MyChar->GetActorLocation());
			//GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Yellow, FString::Printf(TEXT("Overlap Begin OK %f"), DistanceToPlayer));
			if (DistanceToPlayer < Bear->DistanceToPlayerForDamage)
			{
				UGameplayStatics::ApplyDamage(MyChar, Bear->GetMeleeDamage(), nullptr, nullptr, nullptr);
			}
		}
	}

	return EBTNodeResult::Succeeded;
}
