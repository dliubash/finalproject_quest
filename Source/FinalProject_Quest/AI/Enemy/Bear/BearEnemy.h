// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AI/Enemy/EnemyChar.h"
#include <Components/CapsuleComponent.h>
#include <Components/PrimitiveComponent.h>
#include "BearEnemy.generated.h"

/**
 *
 */
UCLASS()
class FINALPROJECT_QUEST_API ABearEnemy : public AEnemyChar
{
	GENERATED_BODY()
public:

	ABearEnemy();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UCapsuleComponent* AttackCapsuleComp;
	UPROPERTY(EditDefaultsOnly, Category = "Sockets")
		FName AttackSocket;

	bool GetIsMeleeAttacking() const { return bIsMeleeAttacking; }
	void SetIsMeleeAttacking(bool val) { bIsMeleeAttacking = val; }
	float GetMeleeDamage() const { return MeleeDamage; }
	void SetMeleeDamage(float val) { MeleeDamage = val; }

	UPROPERTY(EditDefaultsOnly, Category = "Damage")
		float DistanceToPlayerForDamage = 100.0f;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
		void OnOverlapApplyMeleeDamage(UPrimitiveComponent* HitComp, AActor * Other, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex,
			bool bFromSweep, const FHitResult & Hit);
	UFUNCTION()
		void OnOverlapMeleeEnd(UPrimitiveComponent* HitComp, AActor * Other, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex);
	//	void OnOverlapEnd(AActor * Other, AActor * Other2);
	bool bIsWeaponOverlappingEnemy = false;

	UPROPERTY(EditDefaultsOnly, Category = "Damage")
		float MeleeDamage = 20;

	UPROPERTY(BlueprintReadWrite, Category = "Attack")
		bool bIsMeleeAttacking = false;
};
