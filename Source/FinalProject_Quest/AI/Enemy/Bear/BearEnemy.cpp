// Fill out your copyright notice in the Description page of Project Settings.


#include "BearEnemy.h"
#include <Components/CapsuleComponent.h>
#include "MyCharacter/Yin.h"
#include <Kismet/GameplayStatics.h>
#include "../EnemyChar.h"
#include <Components/PrimitiveComponent.h>

ABearEnemy::ABearEnemy()
{

	AttackCapsuleComp = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CapsuleAttackCollision"));
	AttackCapsuleComp->InitCapsuleSize(300, 210);//InitCapsuleSize(100, 70);

	//GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Red, TEXT("Hoho"));
	//if (!AttackSocket.IsNone()) {
	AttackCapsuleComp->SetupAttachment(GetMesh(), AttackSocket);
	//GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Red, TEXT("Ho"));
	//AttackCapsuleComp->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, AttackSocket);
//}

}

void ABearEnemy::BeginPlay()
{
	Super::BeginPlay();


	if (AttackCapsuleComp)
	{

		AttackCapsuleComp->OnComponentBeginOverlap.AddDynamic(this, &ABearEnemy::OnOverlapApplyMeleeDamage);
		AttackCapsuleComp->OnComponentEndOverlap.AddDynamic(this, &ABearEnemy::OnOverlapMeleeEnd);
	}

}

void ABearEnemy::OnOverlapApplyMeleeDamage(UPrimitiveComponent* HitComp, AActor * Other, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & Hit)
{
	if (Cast<AYin>(Other))
	{
		//GEngine->AddOnScreenDebugMessage(-1, 0.5, FColor::Red, TEXT("Overlap Begin"));
		if (Cast<AYin>(Other)->GetCapsuleComponent() == OtherComp)
		{
			//GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Yellow, FString::Printf(TEXT("Overlap Begin OK %d"), GetIsMeleeAttacking()));
			if (/*(!bIsWeaponOverlappingEnemy) &&*/ GetIsMeleeAttacking() == true && VitalityComp->IsAlive())
			{
				//GEngine->AddOnScreenDebugMessage(-1, 0.5, FColor::Red, TEXT("Overlap Begin OK"));
				//bIsWeaponOverlappingEnemy = true;
				UGameplayStatics::ApplyDamage(Other, GetMeleeDamage(), nullptr, this, nullptr);
			}
		}
	}
}

void ABearEnemy::OnOverlapMeleeEnd(UPrimitiveComponent* HitComp, AActor * Other, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{
	if (Cast<AYin>(Other))
	{
		if (Cast<AYin>(Other)->GetCapsuleComponent() == HitComp)
		{
			/*if (bIsWeaponOverlappingEnemy)
			{
				bIsWeaponOverlappingEnemy = false;
			}*/
		}

	}
}
