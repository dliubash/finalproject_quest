// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTaskNode_SetIsAttacking.h"
#include <AIController.h>
#include "BearEnemy.h"
#include <Kismet/GameplayStatics.h>

EBTNodeResult::Type UBTTaskNode_SetIsAttacking::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	AAIController* AIOwner = OwnerComp.GetAIOwner(); // to get the controller
	APawn* Pawn = AIOwner->GetPawn(); // to get the actor

	if (ABearEnemy* Enemy = Cast<ABearEnemy>(Pawn))
	{
		Enemy->SetIsMeleeAttacking(true);
		//GEngine->AddOnScreenDebugMessage(-1, 0.5, FColor::Red, TEXT("SET ATTACK"));
	}

	return EBTNodeResult::Succeeded;
}
