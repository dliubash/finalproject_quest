// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyChar.h"
#include "Vitality/VitalityComp.h"
#include "MyCharacter/XP/XPComponent.h"
#include "MyCharacter/Yin.h"

AEnemyChar::AEnemyChar()
{
	VitalityComp = CreateDefaultSubobject<UVitalityComp>(TEXT("VitalityComponent"));
	//VitalityComp->OnOwnerDied.AddDynamic(this, &AYin::CharacterDied);
	//AActor* DamageCauser;
	VitalityComp->OnOwnerDiedCauser.AddDynamic(this, &AEnemyChar::CharacterDied);
}


void AEnemyChar::CharacterDied(AActor* DamageCauser)
{
	AYin* YinChar = Cast<AYin>(DamageCauser);
	if (YinChar)
	{
		YinChar->XPComponent->AddXPPoints(XPForKill);
		OnCharacterDiedQuestManager(YinChar);
	}
	//GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Yellow, TEXT("Dead .."));
}
