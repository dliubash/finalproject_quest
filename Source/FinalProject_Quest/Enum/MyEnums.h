// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "MyEnums.generated.h"

UENUM(BlueprintType)
enum class EQuestCategories : uint8
{
	QC_MainQuest UMETA(DisplayName = "MainQuest"),
	QC_SideQuest UMETA(DisplayName = "SideQuest"),
	QC_Events UMETA(DisplayName = "Events")
};

UENUM(BlueprintType)
enum class EGoalTypes : uint8
{
	GT_Custom UMETA(DisplayName = "Custom"),
	GT_Hunt UMETA(DisplayName = "Hunt"),
	GT_Find UMETA(DisplayName = "Find"),
	GT_Talk UMETA(DisplayName = "Talk")
};

UENUM(BlueprintType)
enum class ERegions : uint8
{
	Reg_FirstZone UMETA(DisplayName = "FirstZone"),
	Reg_AdventureLand UMETA(DisplayName = "AdventureLand")
};

UENUM(BlueprintType)
enum class EQuestStates : uint8
{
	QS_CurrentQuests UMETA(DisplayName = "CurrentQuests"),
	QS_CompletedQuests UMETA(DisplayName = "CompletedQuests"),
	QS_FailedQuests UMETA(DisplayName = "FailedQuests")
};

UENUM(BlueprintType)
enum class EGoalStates : uint8
{
	GS_Current UMETA(DisplayName = "Current"),
	GS_Success UMETA(DisplayName = "Success"),
	GS_Failed UMETA(DisplayName = "Failed")
};

UENUM(BlueprintType)
enum class EAbilityNames : uint8
{
	AN_AnimalFriend UMETA(DisplayName = "AnimalFriend"),
	AN_Autoregeneration UMETA(DisplayName = "Autoregeneration"),
	AN_WeaponUsage1 UMETA(DisplayName = "WeaponUsage1"),
	AN_WeaponUsage2 UMETA(DisplayName = "WeaponUsage2"),
	AN_HouseBack UMETA(DisplayName = "HouseBack")
};


UENUM(BlueprintType)
enum class EBackpackAbilities : uint8
{
	BA_AddSpeed UMETA(DisplayName = "Add Speed"),
	BA_AddMaxHealth UMETA(DisplayName = "Add Max Health"),
	BA_IncreaseMeleeDamage UMETA(DisplayName = "Increase Melee Damage")
};

USTRUCT(BlueprintType)
struct FSQuestReward
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Experience = 0;
	//	UPROPERTY()
	//		int PrestigePoints = 0;
};

USTRUCT(BlueprintType)
struct FSLocationInfo
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bHasLocation = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector Location;
};

USTRUCT(BlueprintType)
struct FSGoalInfo
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EGoalTypes Type = EGoalTypes::GT_Custom;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bIsCustomGoal = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText GoalText;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText AddtionalName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int AmountToHunt = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<AActor> GoalClass;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int GoalId = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FSLocationInfo LocationInfo;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bIsRadiusUsed = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Radius = 500;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FLinearColor CircleColor = { 1,1,1,1 };
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bUpdateQuestDescription = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText UpdatedDescription;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<int32> FollowingSubGoalIndices;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bDoesFailMeansQuestFail = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bIsQuestCompleted = false;
};

USTRUCT(BlueprintType)
struct FSQuestInfo
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText Name;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EQuestCategories Gategory = EQuestCategories::QC_MainQuest;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText Description;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		ERegions Region = ERegions::Reg_AdventureLand;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FSQuestReward CompletedReward;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int SuggestedLevel = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Difficulty = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FSGoalInfo> Subgoals;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool HasClient = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class AQuestChar> ClientClass;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int ClientId = 0;
};

USTRUCT(BlueprintType)
struct FSCompletedGoal
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int GoalIndex = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FSGoalInfo GoalInfo;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bSuccessful;
};

USTRUCT(BlueprintType)
struct FSIndexToComplete
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class AMasterQuest* Quest;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Index = 0;
};



USTRUCT(BlueprintType)
struct FSAbility
{
	GENERATED_BODY()

public:

	FSAbility()	{}

	FSAbility(int NewLevel, EAbilityNames NewAbilityName, bool bNewDescrShouldBeAddedToControls, 
		FString NewDescription, FString NewLongDescriptionShownOnce)
	{
		Level = NewLevel;
		AbilityName = NewAbilityName;
		bDescrShouldBeAddedToControls = bNewDescrShouldBeAddedToControls;
		Description = FText::AsCultureInvariant(NewDescription);
		LongDescriptionShownOnce = FText::AsCultureInvariant(NewLongDescriptionShownOnce);
	}

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Level = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EAbilityNames AbilityName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bDescrShouldBeAddedToControls;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText Description;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText LongDescriptionShownOnce;
	// changing vars just does not work
	//UPROPERTY(BlueprintReadWrite)
	//	bool bWasActivated = false;
};


USTRUCT(BlueprintType)
struct FSAbilitiesInfo
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FSAbility> Abilities;
};



USTRUCT(BlueprintType)
struct FSBackpackAbility
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EBackpackAbilities BackpackAbilityName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Strength;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText Description;
};

USTRUCT(BlueprintType)
struct FSBackpackAbilitiesInfo
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FSBackpackAbility> BackpackAbilities;
};

UCLASS()
class FINALPROJECT_QUEST_API UMyEnums : public UObject
{
	GENERATED_BODY()

};
