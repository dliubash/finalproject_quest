// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MyEnums.h"

#include "MyActor.generated.h"

UCLASS()
class FINALPROJECT_QUEST_API AMyActor : public AActor
{
	GENERATED_BODY()


public:
	// Sets default values for this actor's properties
	AMyActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintImplementableEvent)
		int ReturnThreeTest(int v);

	UFUNCTION()
		void PrintTest();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Enum)
		EGoalTypes GoalTypes;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
