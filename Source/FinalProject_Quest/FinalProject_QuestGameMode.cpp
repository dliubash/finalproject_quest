// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "FinalProject_QuestGameMode.h"
#include "FinalProject_QuestCharacter.h"
#include "UObject/ConstructorHelpers.h"

AFinalProject_QuestGameMode::AFinalProject_QuestGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
