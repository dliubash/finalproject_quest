// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class FinalProject_Quest : ModuleRules
{
	public FinalProject_Quest(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
        PublicDependencyModuleNames.AddRange(new string[] { "Json", "JsonUtilities" });
        PublicDependencyModuleNames.AddRange(new string[] { "AIModule", "GameplayTasks" });
    }
}
